package pprof

import (
	"fmt"
	"gitlab.com/flex_comp/comp"
	"gitlab.com/flex_comp/conf"
	"gitlab.com/flex_comp/log"
	"gitlab.com/flex_comp/util"
	"net/http"
	_ "net/http/pprof"
	"time"
)

func init() {
	comp.RegComp(new(PProf))
}

type PProf struct{}

func (p *PProf) Init(execArgs map[string]interface{}, _ ...interface{}) error {
	if open := util.ToBool(conf.Get("pprof.open")); !open {
		return nil
	}

	ip := util.ToString(conf.Get("pprof.ip"))
	beginPort := util.ToInt(conf.Get("pprof.begin_port"))
	endPort := util.ToInt(conf.Get("pprof.end_port"))

	go func() {
		for {
			for i := beginPort; i < endPort; i++ {
				addr := fmt.Sprintf("%s:%d", ip, i)
				log.Info("启动性能监听在: ", addr)
				err := http.ListenAndServe(addr, nil)
				if err != nil {
					log.Error("性能监听失败, 尝试更换端口", err)
				}

				<-time.After(time.Millisecond * 100)
			}

			log.Info("端口监听范围用尽, 5s后重新尝试监听")
			<-time.After(time.Second * 5)
		}
	}()

	return nil
}

func (p *PProf) Start(_ ...interface{}) error {
	return nil
}

func (p *PProf) UnInit() {

}

func (p *PProf) Name() string {
	return "pprof"
}
