module gitlab.com/flex_comp/pprof

go 1.16

require (
	gitlab.com/flex_comp/comp v0.1.3 // indirect
	gitlab.com/flex_comp/conf v0.0.0-20210729190352-7eb0a9e754bc // indirect
	gitlab.com/flex_comp/log v0.0.0-20210729190650-519dfabf348e // indirect
	gitlab.com/flex_comp/util v0.0.0-20210729132803-de11a044b5ed // indirect
)
