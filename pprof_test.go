package pprof

import (
	"gitlab.com/flex_comp/comp"
	"testing"
	"time"
)

func TestPProf(t *testing.T) {
	t.Run("test_pprof", func(t *testing.T) {
		_ = comp.Init(map[string]interface{}{
			"env":    "debug",
			"id":     1,
			"config": "./test/server.json",
		})

		ch := make(chan bool, 1)
		go func() {
			<-time.After(time.Second * 10)
			ch <- true
		}()

		_ = comp.Start(ch)
	})
}
